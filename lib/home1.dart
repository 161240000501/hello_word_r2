import 'package:flutter/material.dart';

void main(){
  runApp(new MaterialApp(
    title: "Jepara Kos Application",
    debugShowCheckedModeBanner: false,
    home: new HalamanSatu(),
  )
  );
}

class HalamanSatu extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: new AppBar(
      backgroundColor: Colors.red[700],
      leading: new Icon(Icons.home),
      title: new Center
        (child: new Text("Jepara Kos"),
        ),
      actions: <Widget>[
        new Icon(Icons.close)
        
      ],
    ),
    );
  }
  
}
